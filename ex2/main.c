/* 
    Exercise 2:  Overflow, Hex and Functions

    The goal is to explore how different types (e.g. int, char) have
    a maximum. It is also to explore the difference between signed
    and unsigned. We can do this easily using hexadecimal.

    To compile:
        gcc -ansi -W -pedantic -o main main.c

    Before you do, read about gcc some more, or if 
    you are on Linux/OSX run:
        man gcc
*/


#include <stdio.h>

int Factorial(int n); /* Related to Superbonus */

int main(int argc, char* argv[]) {
    
    /* Our variables */
    char charNum;
    unsigned char ucharNum;

    short shortNum;
    unsigned short ushortNum;

    int intNum;
    unsigned int uintNum;

    /* A char is 8-bit, therefore it can only hold 2^8 (256) numbers */
    charNum = 127;
    printf ("The maximum of a signed char %hhd\n", charNum);

    charNum = -128;
    printf ("The minimum of a signed char %hhd\n", charNum);

    /* We used %d to show the number, instead of %c */    

    /* another way to set the maximum is to use HEX */
    charNum = 0x7F;
    printf ("The maximum of a signed char %hhd\n", charNum);    

    charNum = 0x80;
    printf ("The minimum of a signed char %hhd\n", charNum);

    /* 
        Exercise A
            this can be done for all the variables above, including unsigned,
            print them all out, the first is done for you!
            
            Left to do:
            + shortNum
            + ushortNum
            + intNum
            + uintNum
     */

    ucharNum = 0xFF;
    printf ("\nThe maximum of a unsigned char %hhu\n", ucharNum);

    printf("\n");
    return 0;
}
/*
    Bonus Exercise:

        Given what you've just done, what do you think is the most appropriate variable to use,
        for the situations below? Think about discrete vs continuos values (int vs float) as well,
        give a reason as well :)

        e.g.
        Q0: A person's age?
        A0: unsigned char

        Q1: The amount of people on earth?
        A1: 

        Q2: The difference between two people's ages?
        A2:

        Q3: The height in mm of a person
        A3: 

        Q4: The amount of words in a book
        A4:

        Q5: The speed of a car
        A5:

        Q6: The distance pluto is from the sun
        A6: 
*/

/* 
    Super Bonus Exercise

        At the top is this code:
            int Factorial(int n);

        This is known as a function declaration, it has been declared but not defined
        To define it we need to either add the function code at the top there, or
        add it down here:

            int Factorial(int n) {
                //CODE
                return x;
            }

        The exercise is to write Factorial and then work out what is the largest number,
        generated from Factorial(n) before you get a negative? Make it so you have a
        loop to work this out, and the loop stops at the first negative.
*/

int Factorial(int n) {
}
